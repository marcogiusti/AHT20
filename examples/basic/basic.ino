/*
  AHT20

  Read temperature and humidity from an AHT20 sensor when pressing a
  push button attached to pin 2.

  The circuit:
  - LED attached from pin 13 to ground through 220 ohm resistor
  - Push button attached to pin 2 from +5V
  - 10K resistor attached to pin 2 from ground

  Note: on most Arduinos there is already an LED on the board attached
  to pin 13.

  Created 2023
  by Marco Giusti <http://gitlab.com/marcogiusti>

  This example is based on the button built-in example from DojoDave.

  The code is in the public domain.
*/

#include "aht20.h"

const int buttonPin = 2; // the number of the pushbutton pin
const int ledPin = 13;   // the number of the LED pin

// Create a new instance of TwoWire or just use the default Wire
// instance.
TwoWire wire = TwoWire();
// Create a new instance of AHT20 to control the senson. Default address
// (0x38) is used.
AHT20 aht20 = AHT20(wire);

void setup() {
  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT);

  Serial.begin(115200);
  while (!Serial) {
    // Wait for USB serial port to connect. Needed for native USB port only
  }

  // Do not forget to start the TwoWire instance with the begin()
  // method. Also call AHT20::init() to initialize the sensor.
  Serial.print("Setup AHT20... ");
  wire.begin();
  aht20.init();
  while (aht20.busy() || !aht20.calibrated()) {
    delay(20);
  }
  Serial.println("done");
}

void loop() {
  // read the state of the pushbutton value
  int buttonState = digitalRead(buttonPin);
  // if the pushbutton  is pressed, the buttonState is HIGH
  digitalWrite(ledPin, buttonState);

  if (buttonState == HIGH) {
    // Aquire a new measure from the sensor
    AHT20::Measure sample = aht20.measure();
    // Check if the the sample has been read correctly
    if (sample.ok) {
      Serial.print("temperature: ");
      // Temperature is in Celsius, range -40-85 °C
      Serial.print(sample.temperature);
      Serial.print(" °C, (");
      // Convert the temperature in Fahrenheit (°F)
      Serial.print(aht20.to_fahrenheit(sample.temperature));
      Serial.print(" °F), ");
      Serial.print("humidity: ");
      // Relative humidity, range 0-100 %
      Serial.print(sample.humidity);
      Serial.println(" %RH");
    }
    delay(200);
  }
}
