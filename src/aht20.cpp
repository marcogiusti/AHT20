#include "aht20.h"

#define ATH20_CMD_INIT 0xBE
#define ATH20_CMD_INIT_PARAMS_1ST 0x08
#define ATH20_CMD_INIT_PARAMS_2ND 0x00
#define ATH20_CMD_INIT_DELAY_TIME 10
#define ATH20_CMD_MEASUREMENT 0xAC
#define ATH20_CMD_MEASUREMENT_PARAMS_1ST 0x33
#define ATH20_CMD_MEASUREMENT_PARAMS_2ND 0x00
#define ATH20_CMD_MEASUREMENT_DELAY_TIME 80
#define ATH20_CMD_SOFT_RESET 0xBA
#define ATH20_CMD_SOFT_RESET_DELAY_TIME 20
#define ATH20_CMD_STATUS 0x71
#define AHT20_STATUS_CALIBRATED 0x08 // 0000 1000
#define AHT20_STATUS_MODE 0x50       // 0110 0000
#define AHT20_STATUS_BUSY 0x80       // 1000 0000

AHT20::AHT20(TwoWire &wire, uint8_t address = DEFAULT_I2C_ADDR)
    : _wire(&wire), _address(address) {}

uint8_t AHT20::_write(const uint8_t *data, size_t len) {
  _wire->beginTransmission(_address);
  _wire->write(data, len);
  return _wire->endTransmission();
}

uint8_t AHT20::_read(uint8_t *buf, uint8_t size) {
  uint8_t read;

  read = _wire->requestFrom(_address, size);
  for (uint8_t i = 0; i < read; i++) {
    buf[i] = (uint8_t)_wire->read();
  }
  return read;
}

uint8_t AHT20::reset(void) {
  uint8_t cmd[1] = {ATH20_CMD_SOFT_RESET};
  uint8_t err;

  err = _write(cmd, 1);
  if (err) {
    return err;
  }
  delay(ATH20_CMD_SOFT_RESET_DELAY_TIME);
  return 0;
}

AHT20::Status AHT20::get_status(void) {
  uint8_t cmd[1] = {ATH20_CMD_STATUS};
  uint8_t status;
  uint8_t err;

  err = _write(cmd, 1);
  if (err) {
    return Status(err);
  }
  if (_read(&status, 1) != 1) {
    return Status(TRUNCATED_DATA);
  }
  return Status((status & AHT20_STATUS_CALIBRATED) >> 3,
                (status & AHT20_STATUS_MODE) >> 5,
                (status & AHT20_STATUS_BUSY) >> 7);
}

uint8_t AHT20::init(void) {
  uint8_t cmd[3] = {ATH20_CMD_INIT, ATH20_CMD_INIT_PARAMS_1ST,
                    ATH20_CMD_INIT_PARAMS_2ND};
  uint8_t err;

  err = _write(cmd, 3);
  if (err) {
    return err;
  }
  delay(ATH20_CMD_INIT_DELAY_TIME);
  return 0;
}

bool AHT20::busy(void) {
  Status status = get_status();

  if (status.ok) {
    return status.busy;
  } else {
    return false;
  }
}

bool AHT20::calibrated(void) {
  Status status = get_status();

  if (status.ok) {
    return status.calibrated;
  } else {
    return false;
  }
}

AHT20::Sample AHT20::measure(void) {
  uint8_t cmd[3] = {ATH20_CMD_MEASUREMENT, ATH20_CMD_MEASUREMENT_PARAMS_1ST,
                    ATH20_CMD_MEASUREMENT_PARAMS_2ND};
  uint8_t buf[6];
  uint32_t humidity, temperature;
  uint8_t err;

  err = _write(cmd, 3);
  if (err) {
    return Sample(err);
  }
  delay(ATH20_CMD_MEASUREMENT_DELAY_TIME);
  if (_read(buf, 6) != 6) {
    return Sample(TRUNCATED_DATA);
  }

  humidity = buf[1];
  humidity <<= 8;
  humidity |= buf[2];
  humidity <<= 4;
  humidity |= buf[3] >> 4;

  temperature = buf[3] & 0x0F;
  temperature <<= 8;
  temperature |= buf[4];
  temperature <<= 8;
  temperature |= buf[5];

  return Sample(((float)humidity * 100) / 0x100000,
                ((float)temperature * 200 / 0x100000) - 50);
}

bool AHT20::begin(void) {
  _wire->begin();
  if (init() != 0) {
    return false;
  }

  while (busy() || !calibrated()) {
    delay(10);
  }
  return true;
}
