#ifndef _AHT20_H
#define _AHT20_H

#include <Arduino.h>
#include <Wire.h>

class AHT20 {
public:
  const static uint8_t DEFAULT_I2C_ADDR = 0x38;
  const static uint8_t UNKNOWN_ERROR = 0x7F;
  const static uint8_t TRUNCATED_DATA = 0x7E;

  struct Status {
    bool ok;
    union {
      struct {
        uint8_t calibrated : 1;
        uint8_t mode : 2;
        uint8_t busy : 1;
      };
      uint8_t errcode;
    };

    Status(uint8_t calibrated, uint8_t mode, uint8_t busy)
        : ok(true), calibrated(calibrated), mode(mode), busy(busy) {}
    Status(uint8_t errcode) : ok(false), errcode(errcode) {}
  };

  struct Sample {
    bool ok;
    union {
      struct {
        float humidity;
        float temperature;
      };
      uint8_t errcode;
    };

    Sample(float humidity, float temperature)
        : ok(true), humidity(humidity), temperature(temperature) {}
    Sample(uint8_t errcode) : ok(false), errcode(errcode) {}
  };

  AHT20(TwoWire &wire, uint8_t address = DEFAULT_I2C_ADDR);
  uint8_t init(void);
  uint8_t reset(void);
  Status get_status(void);
  Sample measure(void);
  bool busy(void);
  bool calibrated(void);
  // utils
  bool begin(void);
  static float to_fahrenheit(float temp) { return temp * 1.8 + 32; }

private:
  TwoWire *_wire;
  uint8_t _address;

  uint8_t _write(const uint8_t *data, size_t len);
  uint8_t _read(uint8_t *buf, uint8_t size);
};

#endif
